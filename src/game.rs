mod deck;
mod actions;

pub use deck::Card;
pub use actions::{Action, Outcome, ActionIter};

#[derive(Copy, Clone, Debug, Eq, Ord, PartialOrd, PartialEq, Hash)]
pub struct Player(u8);

#[derive(Copy, Clone, Debug, Eq, Ord, PartialOrd, PartialEq, Hash)]
struct PlayerSecret {
    hand_card: Card,
}

#[derive(Copy, Clone, Debug, Eq, Ord, PartialOrd, PartialEq, Hash)]
struct PlayerPublic {
    immune_until: u8,
    eliminated: bool,
}

#[derive(Clone, Debug, Eq, Ord, PartialOrd, PartialEq, Hash)]
pub struct State {
    deck: Vec<Card>,
    extra_card: Option<Card>,
    drawn_card: Option<Card>,
    turn: u8,
    player_secrets: Vec<PlayerSecret>,
    player_publics: Vec<PlayerPublic>,
    history: Vec<Turn>,
}

#[derive(Clone, Debug, Eq, Ord, PartialOrd, PartialEq, Hash)]
pub struct PublicView<'a> {
    deck_len: u8,
    extra_card_exists: bool,
    turn: u8,
    player_publics: &'a [PlayerPublic],
    history: &'a [Turn],
}

#[derive(Clone, Debug, Eq, Ord, PartialOrd, PartialEq, Hash)]
pub struct PlayerView<'a> {
    public: PublicView<'a>,
    hand_card: Card,
    new_card: Card,
}

#[derive(Copy, Clone, Debug, Eq, Ord, PartialOrd, PartialEq, Hash)]
pub struct Turn {
    player: Player,
    action: Outcome,
}

#[derive(Debug)]
struct OpponentIter<'a> {
    p: Player,
    me: Player,
    turn: u8,
    publics: &'a [PlayerPublic],
}

impl<'a> OpponentIter<'a> {
    fn new(me: Player, publics: &'a [PlayerPublic], turn: u8) -> OpponentIter<'a> {
        let mut iter = OpponentIter {
            p: Player(0),
            me,
            turn,
            publics,
        };
        if let Some(idx) = iter.find_target_index(0) {
            iter.p = Player(idx);
        }
        iter
    }

    fn peek(&self) -> Option<Player> {
        if self.p.0 < self.publics.len() as u8 {
            Some(self.p)
        } else {
            None
        }
    }

    fn terminate(&mut self) {
        self.p = Player(self.publics.len() as u8)
    }

    fn find_target_index(&self, start: u8) -> Option<u8> {
        for p_idx in start..(self.publics.len() as u8) {
            let p = self.publics[p_idx as usize];
            if p_idx != self.me.0 && !p.eliminated && !p.immune_until > self.turn {
                return Some(p_idx);
            }
        }
        None
    }
}

impl<'a> Iterator for OpponentIter<'a> {
    type Item = Player;
    fn next(&mut self) -> Option<Player> {
        if self.p.0 == self.publics.len() as u8 {
            return None
        }
        let prev = self.p;
        if let Some(new_idx) = self.find_target_index(self.p.0 + 1) {
            self.p = Player(new_idx);
        } else {
            self.p = Player(self.publics.len() as u8);
        }
        Some(prev)
    }
}

#[cfg(test)]
fn prepare_state() -> PlayerView<'static> {
    let player_pub = vec![
        PlayerPublic {
            immune_until: 0,
            eliminated: false,
        },
        PlayerPublic {
            immune_until: 0,
            eliminated: false,
        },
        PlayerPublic {
            immune_until: 0,
            eliminated: false,
        },
        PlayerPublic {
            immune_until: 0,
            eliminated: false,
        }
    ].leak();

    let pub_view = PublicView {
        deck_len: 10,
        extra_card_exists: true,
        turn: 0,
        player_publics: &player_pub[..],
        history: &[][..],
    };
    PlayerView {
        public: pub_view,
        hand_card: Card::Guard,
        new_card: Card::Prince,
    }
}

#[test]
fn test_opponent_iter_normal() {
    let player_view = prepare_state();

    let mut iter = player_view.iter_opponents();

    assert_eq!(iter.peek(), iter.peek());
    assert_eq!(iter.peek(), Some(Player(1)));
    assert_eq!(iter.peek(), iter.next());
    assert_eq!(iter.peek(), Some(Player(2)));
    assert_eq!(iter.next(), Some(Player(2)));
    assert_eq!(iter.peek(), Some(Player(3)));
    assert_eq!(iter.next(), Some(Player(3)));

    assert_eq!(iter.peek(), None);
    assert_eq!(iter.next(), None);
    assert_eq!(iter.next(), None);
}

impl<'a> PublicView<'a> {
    fn publics(&self, p: Player) -> PlayerPublic {
        self.player_publics[p.0 as usize]
    }

    fn player_count(&self) -> u8 {
        self.player_publics.len() as u8
    }

    fn current_player(&self) -> Player {
        Player(self.turn % self.player_count())
    }
}

impl<'a> PlayerView<'a> {
    pub fn iter_actions(&self) -> ActionIter {
        ActionIter::new(self)
    }

    fn iter_opponents(&self) -> OpponentIter {
        OpponentIter::new(self.public.current_player(), self.public.player_publics, self.public.turn)
    }
}

impl State {
    pub fn new(player_count: u8) -> State {
        let mut deck = deck::new();
        assert!(deck.len() + 1 >= player_count.into());
        let mut player_publics = Vec::with_capacity(player_count.into());
        let mut player_secrets = Vec::with_capacity(player_count.into());
        for _ in 0..player_count {
            player_publics.push(PlayerPublic {
                immune_until: 0,
                eliminated: false,
            });
            player_secrets.push(PlayerSecret {
                hand_card: deck.pop().expect("there will be enough cards"),
            });
        }

        let extra_card = Some(deck.pop().expect("there will be enough cards"));

        State {
            deck,
            extra_card,
            drawn_card: None,
            turn: 0,
            player_publics,
            player_secrets,
            history: Vec::new(),
        }
    }

    fn player_count(&self) -> u8 {
        self.player_publics.len() as u8
    }

    fn current_player(&self) -> Player {
        Player(self.turn % self.player_count())
    }

    fn secrets(&self, p: Player) -> PlayerSecret {
        self.player_secrets[p.0 as usize]
    }

    fn publics(&self, p: Player) -> PlayerPublic {
        self.player_publics[p.0 as usize]
    }

    fn eliminate(&mut self, p: Player) {
        self.player_publics[p.0 as usize].eliminated = true;
    }

    fn draw_card(&mut self, p: Player) -> Card {
        let old = self.secrets(p).hand_card;
        let new = self
            .deck
            .pop()
            .unwrap_or_else(|| self.extra_card.take().unwrap());
        self.player_secrets[p.0 as usize].hand_card = new;
        old
    }

    fn swap_cards(&mut self, p1: Player, p2: Player) {
        let temp = self.player_secrets[p1.0 as usize];
        self.player_secrets[p1.0 as usize] = self.player_secrets[p2.0 as usize];
        self.player_secrets[p2.0 as usize] = temp;
    }

    fn extract_view(&self) -> PublicView<'_> {
        PublicView {
            deck_len: self.deck.len() as u8,
            extra_card_exists: self.extra_card.is_some(),
            turn: self.turn,
            player_publics: self.player_publics.as_slice(),
            history: self.history.as_slice(),
        }
    }

    pub fn start_turn(&mut self) -> PlayerView<'_> {
        let me = self.current_player();
        let drawn_card = self.deck.pop().expect("invariant: start_turn must not be called if game is over");
        self.drawn_card = Some(drawn_card);
        PlayerView {
            public: self.extract_view(),
            hand_card: self.secrets(me).hand_card,
            new_card: drawn_card,
        }
    }

    pub fn iter_players(&self) -> impl Iterator<Item=Player> {
        (0..self.player_publics.len() as u8).map(|idx| Player(idx))
    }

    fn iter_opponents(&self) -> OpponentIter {
        OpponentIter::new(self.current_player(), &self.player_publics, self.turn)
    }

    pub fn iter_alive_players(&self) -> impl Iterator<Item=Player> + '_ {
        self.iter_players().filter(move |p| self.publics(*p).eliminated)
    }

    pub fn is_game_ended(&self) -> Option<Player> {
        let me = self.current_player();
        // Case 1: No other players alive but me
        let mut players = self.iter_alive_players();
        if players.next() == Some(me) && players.next() == None {
            return Some(me);
        }
        // Case 2: Deck is exhausted so select highest ranking player
        if self.deck.len() == 0 {
            return self.iter_alive_players()
                .max_by_key(|p| self.secrets(*p).hand_card.rank());
        }
        None
    }

    pub fn end_turn(&mut self, action: Action) -> Option<Player> {
        let me = self.current_player();
        let drawn_card = self.drawn_card.expect("invariant: drawn_card is set in start_turn");

        // If current hand card was played, replace it with drawn card
        if self.secrets(me).hand_card == action.card() {
            self.player_secrets[me.0 as usize].hand_card = drawn_card;
        }
        self.drawn_card = None;

        // Update the game state
        let outcome= action.act_on(self);

        self.history.push(Turn {
            player: me,
            action: outcome,
        });

        self.turn += 1;

        self.is_game_ended()
    }
}
