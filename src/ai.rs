use crate::game::{Action, PlayerView};

pub fn decide(ps: &PlayerView) -> Action {
    println!("Partial state: {:#?}", ps);
    for a in ps.iter_actions() {
        println!("Action: {:?}", a);
    }
    ps.iter_actions().next().unwrap()
}
