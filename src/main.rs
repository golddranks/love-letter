mod ai;
mod game;

use game::State;

fn main() {
    let mut gs = State::new(4);
    loop {
        println!("Game state: {:#?}", gs);
        let ps = gs.start_turn();
        let action = ai::decide(&ps);
        if let Some(p) = gs.end_turn(action) {
            println!("Player {:?} won!", p);
            break;
        }
    }
}
