use rand::seq::SliceRandom;

#[derive(Copy, Clone, Debug, Eq, Ord, PartialOrd, PartialEq, Hash)]
pub enum Card {
    Princess,
    King,
    Countess,
    Prince,
    Handmaid,
    Baron,
    Priest,
    Guard,
}

impl Card {
    pub fn rank(self) -> u8 {
        use Card::*;
        match self {
            Princess => 8,
            Countess => 7,
            King => 6,
            Prince => 5,
            Handmaid => 4,
            Baron => 3,
            Priest => 2,
            Guard => 1,
        }
    }

    fn total_count(self) -> u8 {
        use Card::*;
        match self {
            Princess => 1,
            Countess => 1,
            King => 1,
            Prince => 2,
            Handmaid => 2,
            Baron => 2,
            Priest => 2,
            Guard => 5,
        }
    }
}

#[derive(Debug)]
pub struct CardKindIter(u8);

pub fn iter_kinds() -> CardKindIter {
    CardKindIter(0)
}

impl Iterator for CardKindIter {
    type Item = Card;
    fn next(&mut self) -> Option<Card> {
        use Card::*;
        let kind = self.0;
        self.0 += 1;
        match kind {
            0 => Some(Princess),
            1 => Some(King),
            2 => Some(Countess),
            3 => Some(Prince),
            4 => Some(Handmaid),
            5 => Some(Baron),
            6 => Some(Priest),
            7 => Some(Guard),
            _ => None,
        }
    }
}

pub fn new() -> Vec<Card> {
    let mut deck = Vec::new();
    for kind in iter_kinds() {
        for _ in 0..kind.total_count() {
            deck.push(kind);
        }
    }
    deck.shuffle(&mut rand_pcg::Lcg64Xsh32::new(
        0xcafef00dd15ea5e5,
        0xa02bdbf7bb3c0a7,
    ));
    deck
}
