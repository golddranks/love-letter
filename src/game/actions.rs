use std::cmp::Ordering;

use super::{deck, Card, Player, OpponentIter, PlayerView, State};

#[derive(Copy, Clone, Debug, Eq, Ord, PartialOrd, PartialEq, Hash)]
pub enum Action {
    Princess,
    King { player: Player },
    Countess,
    Prince { player: Player },
    Handmaid,
    Baron { player: Player },
    Priest { player: Player },
    Guard { player: Player, guess: Card },
}

#[derive(Copy, Clone, Debug, Eq, Ord, PartialOrd, PartialEq, Hash)]
pub enum Outcome {
    Princess,
    King {
        player: Player,
    },
    Countess,
    Prince {
        player: Player,
        discard: Card,
    },
    Handmaid {
        until: u8,
    },
    Baron {
        player: Player,
        eliminated: Option<Player>,
    },
    Priest {
        player: Player,
    },
    Guard {
        player: Player,
        guess: Card,
        eliminated: bool,
    },
}

impl Action {
    pub fn card(&self) -> Card {
        use Action::*;
        match self {
            Princess => Card::Princess,
            King{..} => Card::King,
            Countess => Card::Countess,
            Prince {..} => Card::Prince,
            Handmaid => Card::Handmaid,
            Baron {..} => Card::Baron,
            Priest {..} => Card::Priest,
            Guard { .. } => Card::Guard,
        }
    }

    pub fn act_on(&self, state: &mut State) -> Outcome {
        let me = state.current_player();
        use Action::*;
        match *self {
            Princess => {
                state.eliminate(me);
                Outcome::Princess
            }
            King { player } => {
                state.swap_cards(me, player);
                Outcome::King { player }
            }
            Countess => Outcome::Countess,
            Prince { player } => {
                let discard = state.draw_card(player);
                if discard == Card::Princess {
                    state.eliminate(player);
                }
                Outcome::Prince { player, discard }
            }
            Handmaid => {
                let until = state.turn + state.player_count();
                state.publics(me).immune_until = until;
                Outcome::Handmaid {
                    until,
                }
            },
            Baron { player } => {
                let their_card = state.secrets(player).hand_card;
                let my_card = state.secrets(me).hand_card;
                let eliminate = match their_card.rank().cmp(&my_card.rank()) {
                    Ordering::Greater => Some(me),
                    Ordering::Equal => None,
                    Ordering::Less => Some(player),
                };
                if let Some(p) = eliminate {
                    state.eliminate(p);
                }
                Outcome::Baron {
                    player,
                    eliminated: eliminate,
                }
            }
            Priest { player } => Outcome::Priest { player },
            Guard { player, guess } => {
                let eliminated = state.secrets(player).hand_card == guess;
                if eliminated {
                    state.eliminate(player);
                }
                Outcome::Guard {
                    player,
                    guess,
                    eliminated,
                }
            }
        }
    }
}

#[derive(Debug)]
pub struct ActionIter<'a> {
    view: &'a PlayerView<'a>,
    cards_processed: u8,
    player_iter: OpponentIter<'a>,
    guess: deck::CardKindIter,
}

impl<'a> ActionIter<'a> {
    pub fn new<'v>(view: &'v PlayerView<'_>) -> ActionIter<'v> {
        ActionIter {
            view,
            cards_processed: 0,
            player_iter: view.iter_opponents(),
            guess: deck::iter_kinds(),
        }
    }

    fn process_card(&mut self, card: Card) -> Option<Action> {
        use Card::*;
        Some(match card {
            Princess | Countess | Handmaid => {
                if self.player_iter.peek().is_none() {
                    return None;
                }
                self.player_iter.terminate();
                match card {
                    Princess => Action::Princess,
                    Countess => Action::Countess,
                    Handmaid => Action::Handmaid,
                    _ => unreachable!(),
                }
            }
            King | Prince | Baron | Priest => {
                let player;
                if let Some(p) = self.player_iter.next() {
                    player = p;
                } else {
                    return None;
                };
                match card {
                    King => Action::King { player },
                    Prince => Action::Prince { player },
                    Baron => Action::Baron { player },
                    Priest => Action::Priest { player },
                    _ => unreachable!(),
                }
            }
            Guard => {
                let guess;
                let player;
                if let Some(g) = self.guess.next().filter(|c| *c != Guard) {
                    guess = g;
                    if let Some(p) = self.player_iter.peek() {
                        player = p;
                    } else {
                        return None;
                    }
                } else {
                    self.guess = deck::iter_kinds();
                    guess = self.guess.next().expect("always succeeds");
                    player = self.player_iter.next().expect("always succeeds");
                };
                Action::Guard { player, guess }
            }
        })
    }
}

impl<'a> Iterator for ActionIter<'a> {
    type Item = Action;
    fn next(&mut self) -> Option<Action> {
        loop {
            let card = match self.cards_processed {
                0 => self.view.hand_card,
                1 => {
                    if self.view.hand_card != self.view.new_card {
                        self.view.new_card
                    } else {
                        // The two cards were the same so it doesn't make sense to
                        // re-iterate the same possible actions.
                        return None;
                    }
                }
                _ => return None,
            };
            let a = self.process_card(card);
            if let Some(action) = a {
                return Some(action);
            } else {
                self.cards_processed += 1;
                self.player_iter = self.view.iter_opponents();
            }
        }
    }
}